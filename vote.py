class VoteSystem:
    def __init__(self, topic):
        self.topic = topic
        self.votes = {}

    def vote(self, choice):
        if choice not in self.votes:
            self.votes[choice] = 0
        self.votes[choice] += 1

    def get_results(self):
        total_votes = sum(self.votes.values())
        if total_votes == 0:
            return "No votes yet."
        results = f"Results for '{self.topic}':\n"
        for choice, count in self.votes.items():
            percentage = (count / total_votes) * 100
            results += f"{choice}: {count} votes ({percentage:.2f}%)\n"
        return results
        glpat-ygigQBPs54i4RMnzxs8z

def main():
    topic = input("Enter the topic of the vote: ")
    vote_system = VoteSystem(topic)
    print("Vote by entering your choice (e.g., 'A', 'B', 'C'). Enter 'done' to finish voting.")
    while True:
        choice = input("Your choice: ").upper()
        if choice == 'DONE':
            break
        vote_system.vote(choice)
    print(vote_system.get_results())

if __name__ == "__main__":
    main()
